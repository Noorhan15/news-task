from django.test import SimpleTestCase, TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

# Create your tests here.
class HomePageTests (SimpleTestCase):
    def test_home_status_code(self):
        response = self.client.get("/")
        self.assrtEqual(response.status_code,200)
    def test_by_name (self):
        response = self.client.get(reverse("home"))
        self.assrtEqual(response.status_code,200)
    def test_use_correct_template (self):
        response = self.client.get(reverse("home"))
        self.assrtEqual(response.status_code,200)
        self.assertTemplateUsed(response, "home.html")
class SignupPageTests (TestCase):
    username = "newuser"
    email = "newuser@email.com"
    
    def test_signup_status_code (self):
        response = self.client.get("/users/signup/")
        self.assrtEqual(response.status_code,200)
    def test_by_name (self):
        response = self.client.get(reverse("signup"))
        self.assrtEqual(response.status_code,200)
    def test_uses_correct_template(self):
        response = self.client.get(reverse("signup"))
        self.assrtEqual(response.status_code,200)
        self.assertTemplateUsed(response, "signup.html")
    
    def test_signup_form(self):
        new_user = get_user_model().object.create_user(self.username, self.email)
        self.assrtEqual(get_user_model().object.all().count,1)
        self.assrtEqual(get_user_model().object.all()[0].username, self.username)
        self.assrtEqual(get_user_model().object.all()[0].email, self.email)
        
        
        