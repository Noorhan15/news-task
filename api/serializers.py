from rest_framework import serializers

from books.models import Book 


class BookSerializers(serializers.ModelSerializers):
    class Meta:
        model = Book
        fields = ("title","subtitle","author","isbn")